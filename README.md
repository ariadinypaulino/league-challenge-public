# README #

### What is this repository for? ###

* League Code Challenge

### How to run the code ###

*From the same folder the project is in, run command:

1 - using the jar file: league-challenge-1.0-SNAPSHOT.jar
    java -jar league-challenge-1.0-SNAPSHOT.jar <csv file name>

2 - to build the code
    ./gradlew clean build
    go to the created build folder and then run the command for the jar file in step 1.

3 - to run the unit tests
    ./gradlew clean test --info (use --info to see the output)