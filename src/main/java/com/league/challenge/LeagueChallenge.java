package com.league.challenge;

import java.io.File;

/**
 * Main class
 *
 * @author Ady Paulino
 */
public class LeagueChallenge {

    public static void main(String[] args) {
        // all the logs would be changed to a log4j implementation.
        // did this way to keep it simple
        System.out.println(String.format("Got %s path to process " , args.length));

        if (args.length > 0) {
            FileHandler fileHandler = new FileHandler();
            File file = new File(args[0]);
            System.out.println(String.format("START processing file %s", file));
            fileHandler.printAll(file);
        } else {
            System.out.println("No valid file found to read the values from.");
        }
    }
}
