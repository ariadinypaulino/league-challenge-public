package com.league.challenge;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class to read the file and process the matrix
 *
 * @author Ady Paulino
 */
public class FileHandler {

    private static final String DELIMITER = ",";
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    /**
     * Read the file lines and process according to the challenge
     * @param file - the file to be processed
     */
    private List<String> readLine(File file) {
        try (Stream<String> stream = Files.lines(Paths.get(file.getAbsolutePath()))) {
            //return as a List to reuse the stream
            return stream.collect(Collectors.toList());
        } catch (Exception e) {
            System.out.println(String.format("Error while reading line from file %s ", file));
        }

        return Collections.emptyList();
    }

    public String printMatrixFormat(File file) {
        List<String> stream = readLine(file);
        if (stream.isEmpty()) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        System.out.println("Printing in matrix format");
        //matrix format
        stream.forEach(line -> {
            if (sb.length() > 0) {
                sb.append(LINE_SEPARATOR);
            }
            sb.append(line);
        });
        System.out.println(sb);

        return sb.toString();
    }

    public String printFlattenFormat(File file) {
        List<String> stream = readLine(file);
        if (stream.isEmpty()) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        System.out.println("Printing in flatten format");
        stream.forEach(line -> sb.append(line).append(DELIMITER));
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.lastIndexOf(DELIMITER));
        }
        System.out.println(sb);

        return sb.toString();
    }

    public int printSum(File file) {
        List<String> stream = readLine(file);
        if (stream.isEmpty()) {
            return 0;
        }

        AtomicInteger sum = new AtomicInteger(0);
        //sum the integers
        stream.forEach(line -> {
            String[] values = line.split(DELIMITER);
            for (String value : values) {
                try {
                    int valueInt = Integer.parseInt(value);
                    sum.addAndGet(valueInt);
                } catch (NumberFormatException nfe) {
                    System.out.println(String.format("Error while trying to sum value %s", value));
                }
            }
        });

        System.out.println("Printing the sum");
        System.out.println(sum.get());

        return sum.get();
    }

    public long printProduct(File file) {
        List<String> stream = readLine(file);
        if (stream.isEmpty()) {
            return 0;
        }

        AtomicLong product = new AtomicLong(1);
        //multiply the integers
        stream.forEach(line -> {
            String[] values = line.split(DELIMITER);
            for (String value : values) {
                try {
                    int valueInt = Integer.parseInt(value);
                    product.accumulateAndGet(valueInt, (x, y) -> x * y);
                } catch (NumberFormatException nfe) {
                    System.out.println(String.format("Error while trying to sum value %s", value));
                }
            }
        });

        System.out.println("Printing the product");
        System.out.println(product.get());

        return product.get();
    }

    public String printInverted(File file) {
        List<String> stream = readLine(file);
        if (stream.isEmpty()) {
            return null;
        }

        // the matrix is square, columns and rows are the same value
        int numberOfRows = (int) stream.stream().count();
        String[][] newMatrix = new String[numberOfRows][numberOfRows];

        AtomicInteger col = new AtomicInteger(0);
        //building the new matrix for better visualization
        stream.forEach(line -> {
            String[] values = line.split(DELIMITER);
            for (int row = 0; row < values.length; row++) {
                //making the row as column and column as row
                newMatrix[row][col.get()] = values[row];
            }
            col.addAndGet(1);
        });

        System.out.println("Printing inverted");
        StringBuilder line = new StringBuilder();
        Arrays.stream(newMatrix).forEach(strings -> {
            StringBuilder sb = new StringBuilder();
            if (line.length() > 0) {
                //adding the line separator not to the last item
                line.append(LINE_SEPARATOR);
            }
            Arrays.stream(strings).forEach(s -> {
                if (sb.length() > 0) {
                    sb.append(DELIMITER);
                }
                sb.append(s);
            });
            line.append(sb);
        });

        System.out.println(line);

        return line.toString();
    }

    public void printAll(File file) {
        printMatrixFormat(file);
        printInverted(file);
        printFlattenFormat(file);
        printSum(file);
        printProduct(file);
    }
}
