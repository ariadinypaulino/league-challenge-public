package com.league.challenge;

import java.io.File;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class to test FileHandler methods
 *
 * @author Ady Paulino
 */
class FileHandlerTest {

    public static final String FILES_LOCATION = new File("src/test/resources").getAbsolutePath();

    private final FileHandler fileHandler = new FileHandler();

    @Test
    void printMatrixFormat_WhenFileIsValid_ShouldReturnExpectedResult() {
        //given
        File file = new File(FILES_LOCATION + "/matrix.csv");
        StringBuilder expectedResult = new StringBuilder("1,2,3").append(FileHandler.LINE_SEPARATOR)
                .append("4,5,6").append(FileHandler.LINE_SEPARATOR)
                .append("7,8,9");

        //when
        String result = fileHandler.printMatrixFormat(file);

        //then
        Assertions.assertEquals(expectedResult.toString(), result, "Should print in matrix format");
    }

    @Test
    void printFlattenFormat_WhenFileIsValid_ShouldReturnExpectedResult() {
        //given
        File file = new File(FILES_LOCATION + "/matrix.csv");
        String expectedResult = "1,2,3,4,5,6,7,8,9";

        //when
        String result = fileHandler.printFlattenFormat(file);

        //then
        Assertions.assertEquals(expectedResult, result, "Should print in flatten format");
    }

    @Test
    void printSum_WhenFileIsValid_ShouldReturnExpectedResult() {
        //given
        File file = new File(FILES_LOCATION + "/matrix.csv");
        int expectedResult = 45;

        //when
        int result = fileHandler.printSum(file);

        //then
        Assertions.assertEquals(expectedResult, result, "Should print the sum");
    }

    @Test
    void printProduct_WhenFileIsValid_ShouldReturnExpectedResult() {
        //given
        File file = new File(FILES_LOCATION + "/matrix.csv");
        long expectedResult = 362880;

        //when
        long result = fileHandler.printProduct(file);

        //then
        Assertions.assertEquals(expectedResult, result, "Should print the product");
    }

    @Test
    void printInverted_WhenFileIsValid_ShouldReturnExpectedResult() {
        //given
        File file = new File(FILES_LOCATION + "/matrix.csv");
        StringBuilder expectedResult = new StringBuilder("1,4,7").append(FileHandler.LINE_SEPARATOR)
                .append("2,5,8").append(FileHandler.LINE_SEPARATOR)
                .append("3,6,9");

        //when
        String result = fileHandler.printInverted(file);

        //then
        Assertions.assertEquals(expectedResult.toString(), result, "Should print the matrix inverted");
    }

    @Test
    void printInverted_WhenFileIsEmpty_ShouldReturnNull() {
        //given
        File file = new File(FileHandlerTest.FILES_LOCATION + "/empty_matrix.csv");

        //when
        String result = fileHandler.printInverted(file);

        //then
        Assertions.assertNull(result, "Should return null");
    }

    @Test
    void printMatrixFormat_WhenFileHasOneItem_ShouldReturnExpectedResult() {
        //given
        File file = new File(FileHandlerTest.FILES_LOCATION + "/one_item_matrix.csv");
        String expectedResult = "1";

        //when
        String result = fileHandler.printMatrixFormat(file);

        //then
        Assertions.assertEquals(expectedResult, result, "Should print the matrix");
    }

    @Test
    void printInverted_WhenFileIsNull_ShouldReturnNull() {
        //given
        File file = null;

        //when
        String result = fileHandler.printInverted(file);

        //then
        Assertions.assertNull(result, "Should return null");
    }
}
